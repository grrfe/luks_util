echo "Enter path where key should be created (including filename, PLEASE MAKE SURE YOU TYPE THE CORRECT PATH SINCE IT WILL BE OVERWRITTEN):"
read keyfile

echo "Generating key..."
sudo openssl genrsa -out $keyfile 5120

echo "Enter device (e.g. /dev/sda):"
read device

echo "Creating luks volume..."
sudo cryptsetup --cipher aes-xts-plain64 --key-size 512 --hash sha512 --key-file=$keyfile -v luksFormat $device

echo "Enter mapper name (just the name, e.g. hdd_crypt):"
read mapper_name

echo "Opening luks volume..."
sudo cryptsetup luksOpen $device $mapper_name --key-file=$keyfile

echo "Creating ext4 partition..."
sudo mkfs.ext4 "/dev/mapper/$mapper_name"

echo "Reading UUID of device..."
deviceUUID=$(sudo cryptsetup luksDump $device | grep "UUID" | sed "s/UUID://g" | sed "s/^[ \t]*//;s/[ \t]*$//")
echo "Read UUID is $deviceUUID"

crypttabBackup="/etc/crypttab_$(date +%s).backup"
echo "Backing up crypttab to $crypttabBackup.."
sudo cp /etc/crypttab $crypttabBackup 

echo "Adding crypttab entry..."
echo "$mapper_name UUID=$deviceUUID $keyfile luks" | sudo tee -a /etc/crypttab

echo "Input mountpoint (e.g. /mnt/hdd):"
read mount_point

fstabBackup="/etc/fstab_$(date +%s).backup"
echo "Backing up fstab to $fstabBackup.."
sudo cp /etc/fstab $fstabBackup

echo "Adding fstab entry..."
echo "/dev/mapper/$mapper_name $mount_point ext4 auto,nofail,noatime 0 2" | sudo tee -a /etc/fstab

echo "Done!"





