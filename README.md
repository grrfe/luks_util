# luks_util

A collection of util scripts for luks.

Scripts:

* [setup_keyed_luks_disk.sh](setup_keyed_luks_disk.sh): Creates new Luks volume with an ext4 filesystem. Handles everything from creating the key (generates a `5120` bit long RSA key), setting up the volume (cipher: `aes-xts-plain64`, keysize: `512`, hash: `sha512`), creating the `ext4` filesystem to adding required entries to `/etc/fstab` and `/etc/crypttab` for automounting/unlocking (backups are made before any changes are made to these files). **Use with caution!**
